package com.itheima.bos.web.action.base;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Controller;

import com.itheima.bos.domain.base.Courier;
import com.itheima.bos.domain.base.Standard;
import com.itheima.bos.service.base.CourierService;
import com.itheima.bos.web.action.CommonAction;

import net.sf.json.JsonConfig;

/**
 * ClassName:CourierAction <br/>
 * Function: <br/>
 * Date: 2018年3月14日 下午8:03:21 <br/>
 */
@Namespace("/")
@ParentPackage("struts-default")
@Controller
@Scope("prototype")
public class CourierAction extends CommonAction<Courier> {


    public CourierAction() {
        super(Courier.class);  
    }

    @Autowired
    private CourierService courierService;

    @Action(value = "courierAction_save", results = {
            @Result(name = "success", location = "/pages/base/courier.html", type = "redirect")})
    public String save() {
        courierService.save(getModel());
        return SUCCESS;
    }


    @Action("courierAction_pageQuery")
    public String pageQuery() throws IOException {

        Specification<Courier> specification = new Specification<Courier>() {

            @Override
            public Predicate toPredicate(Root<Courier> root, CriteriaQuery<?> query,
                    CriteriaBuilder cb) {
                String courierNum = getModel().getCourierNum();
                Standard standard = getModel().getStandard();
                String company = getModel().getCompany();
                String type = getModel().getType();
                List<Predicate> list = new ArrayList<Predicate>();
                if (StringUtils.isNotEmpty(courierNum)) {
                    //如果工号不为空,构建一个等值查询语句
                    Predicate p1 = cb.equal(root.get("courierNum").as(String.class), courierNum);
                    list.add(p1);
                }
                if (standard != null) {
                    String name = standard.getName();
                    if (StringUtils.isNotEmpty(name)) {
                        //连表查询,查询标准的名字
                        Join<Object, Object> join = root.join("standard");
                        Predicate p2 = cb.equal(join.get("name").as(String.class), name);
                        list.add(p2);
                    }
                }
                
                if (StringUtils.isNotEmpty(company)) {
                  //如果所属单位不为空,构建一个模糊查询语句
                    Predicate p3 = cb.like(root.get("company").as(String.class), "%"+company+"%");
                    list.add(p3);
                }
                if (StringUtils.isNotEmpty(type)) {
                  //如果类型不为空,构建一个等值查询语句
                    Predicate p4 = cb.equal(root.get("type").as(String.class), type);
                    list.add(p4);
                }
                
                //用户没有输入查询条件
                if (list.size() == 0) {
                    return null;
                }
                //用户输入了查询条件
                Predicate[] arr = new Predicate[list.size()];
                list.toArray(arr);
                Predicate predicate = cb.and(arr);
                return predicate;
            }
        };

        Pageable pageable = new PageRequest(page - 1, rows);
        Page<Courier> page = courierService.findAll(specification,pageable);

        JsonConfig jsonConfig = new JsonConfig();
        jsonConfig.setExcludes(new String[] {"fixedAreas", "takeTime"});
        
        page2json(page, jsonConfig);
        return NONE;
    }

    // 使用属性驱动获取要删除的快递员的id
    private String ids;

    public void setIds(String ids) {
        this.ids = ids;
    }

    @Action(value = "courierAction_batchDel", results = {
            @Result(name = "success", location = "/pages/base/courier.html", type = "redirect")})
    public String batchDel() {
        courierService.batchDel(ids);
        return SUCCESS;
    }
}
