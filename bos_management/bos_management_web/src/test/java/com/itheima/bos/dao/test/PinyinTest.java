package com.itheima.bos.dao.test;

import com.itheima.utils.PinYin4jUtils;

/**
 * ClassName:PinyinTest <br/>
 * Function: <br/>
 * Date: 2018年3月16日 下午3:23:33 <br/>
 */
public class PinyinTest {

    public static void main(String[] args) {
        String provice = "广东省";
        String city = "深圳市";
        String district = "宝安区";
        provice = provice.substring(0, provice.length() - 1);
        city = city.substring(0, city.length() - 1);
        district = district.substring(0, district.length() - 1);

        /*String case1 = PinYin4jUtils.hanziToPinyin(city, "").toUpperCase();
        System.out.println(case1);*/
        
        String[] headByString = PinYin4jUtils.getHeadByString(provice+city+district);
        String arrayToString = PinYin4jUtils.stringArrayToString(headByString);
        System.out.println(arrayToString);
    }

}
