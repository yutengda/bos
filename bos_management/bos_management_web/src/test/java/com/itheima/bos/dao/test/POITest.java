package com.itheima.bos.dao.test;

import java.io.FileInputStream;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**  
 * ClassName:POITest <br/>  
 * Function:  <br/>  
 * Date:     2018年3月15日 下午9:19:15 <br/>       
 */
public class POITest {

    public static void main(String[] args) throws Exception {
        //读取文件
        HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream("C:\\Users\\yutengda\\桌面\\a.xls"));
        //读取sheet
        HSSFSheet sheet = workbook.getSheetAt(0);
        //遍历每一行
        for (Row row : sheet) {
            int i = row.getRowNum();
            if (i == 0) {
                continue;
            }
            //遍历每一列
            for (Cell cell : row) {
                String value = cell.getStringCellValue();
                System.out.print(value+"\t");
            }
            System.out.println();
        }
        //关闭资源
        workbook.close();
    }

}
  
