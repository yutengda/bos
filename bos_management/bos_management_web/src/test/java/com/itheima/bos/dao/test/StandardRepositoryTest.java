package com.itheima.bos.dao.test;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.itheima.bos.dao.base.StandardRepository;
import com.itheima.bos.domain.base.Standard;

/**  
 * ClassName:StandardRepositoryTest <br/>  
 * Function:  <br/>  
 * Date:     2018年3月12日 下午7:42:51 <br/>       
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class StandardRepositoryTest {

    @Autowired
    private StandardRepository standardRepository;
    
    //查所有
    @Test
    public void test1() {
        List<Standard> list = standardRepository.findAll();
        for (Standard standard : list) {
            System.out.println(standard);
        }
    }
    
    //增
    @Test
    public void test2() {
        Standard standard = new Standard();
        standard.setName("李四");
        standard.setMinWeight(100);
        standardRepository.save(standard);
    }
    
    //该
    @Test
    public void test3() {
        Standard standard = new Standard();
        standard.setId(2L);
        standard.setName("李四");
        standard.setMinWeight(1000);
        //save具有添加和修改功能
        //想修改必须传入id
        standardRepository.save(standard);
    }
    
    //查一条
    @Test
    public void test4() {
        Standard standard = standardRepository.findOne(2L);
        System.out.println(standard);
    }
    
    //删
    @Test
    public void test5() {
        standardRepository.delete(2L);
    }
    
    //自定义方法
    @Test
    public void test6() {
        List<Standard> list = standardRepository.findByName("张三");
        for (Standard standard : list) {
            System.out.println(standard);
        }
    }
    
    @Test
    public void test7() {
        List<Standard> list = standardRepository.findByNameLike("%张三%");
        for (Standard standard : list) {
            System.out.println(standard);
        }
    }
    
    @Test
    public void test8() {
        List<Standard> list = standardRepository.findByNameLikeAndMinWeight("张三",100);
        for (Standard standard : list) {
            System.out.println(standard);
        }
    }
    
    @Test
    public void test9() {
        List<Standard> list = standardRepository.findByNameLikeAndMinWeight132("张三",100);
        for (Standard standard : list) {
            System.out.println(standard);
        }
    }
    
    @Test
    public void test10() {
        List<Standard> list = standardRepository.findByNameLikeAndMinWeight132(100,"张三");
        for (Standard standard : list) {
            System.out.println(standard);
        }
    }
    
    @Test
    public void test11() {
        List<Standard> list = standardRepository.findByNameLikeAndMinWeight132131("张三",100);
        for (Standard standard : list) {
            System.out.println(standard);
        }
    }
    
    //在测试用例中使用事务注解,方法执行后,事务回滚了,要想有效果,要在方法接口中使用
    @Test
    public void test12() {
        standardRepository.updateWeightByName(200, "张三");
    }
    
    //在测试用例中使用事务注解,方法执行后,事务回滚了,要想有效果,要在方法接口中使用
    @Test
    public void test13() {
        standardRepository.deleteByName("张三");
    }

}
  
