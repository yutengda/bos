package com.itheima.bos.dao.base;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.itheima.bos.domain.base.Courier;

/**
 * ClassName:CourierRepository <br/>
 * Function: <br/>
 * Date: 2018年3月14日 下午8:12:31 <br/>
 */
//JpaSpecificationExecutor接口不能单独使用,一般和JpaRepository接口一起使用
public interface CourierRepository
        extends JpaRepository<Courier, Long>, JpaSpecificationExecutor<Courier> {

    @Modifying
    @Query("update Courier set deltag = 1 where id = ?")
    void updateDelTagById(Long id);
}
