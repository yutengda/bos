package com.itheima.bos.dao.base;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.itheima.bos.domain.base.Standard;

/**
 * ClassName:StandardRepository <br/>
 * Function: <br/>
 * Date: 2018年3月12日 下午7:38:51 <br/>
 */
// 泛型1:封装数据的对象的类型
// 泛型2:对象的主键的类型
public interface StandardRepository extends JpaRepository<Standard, Long> {
    // SpringDataJPA提供了一套命名规范,遵循这一套规范定义查询类方法
    // 定义方法是必须以findBy开头,后面跟属性名字,首字母必须大写
    // 如果有多个条件,使用必须对应的SQL关键字
    List<Standard> findByName(String name);

    List<Standard> findByNameLike(String name);

    List<Standard> findByNameLikeAndMinWeight(String name, Integer minWeight);

    @Query("from Standard where name=? and minWeight=?") // 查询语句:JPQL == HQL
    // 自定义方法,不按照SQL关键字
    List<Standard> findByNameLikeAndMinWeight132(String name, Integer minWeight);

    @Query("from Standard where name=?2 and minWeight=?1") // 查询语句:JPQL == HQL
    // 自定义方法,不按照SQL关键字
    List<Standard> findByNameLikeAndMinWeight132(Integer minWeight, String name);

    @Query(value = "select * from T_STANDARD where C_NAME=? and C_MIN_WEIGHT=?", nativeQuery = true)
    // 使用原生SQL语句
    List<Standard> findByNameLikeAndMinWeight132131(String name, Integer minWeight);

    // 更改类的操作必须加一个@Modifying注解
    @Modifying
    @Transactional
    @Query("update Standard set minWeight=? where name=?")
    // 更改必须指定SQL语句
    void updateWeightByName(Integer minWeight, String name);
    
    // 更改类的操作必须加一个@Modifying注解
    @Modifying
    @Transactional
    @Query("delete from Standard where name=?")
    // 删除必须指定SQL语句
    void deleteByName(String name);
}
