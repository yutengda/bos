package com.itheima.bos.dao.base;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.itheima.bos.domain.base.FixedArea;
import com.itheima.bos.domain.base.SubArea;

/**
 * ClassName:SubAreaRepository <br/>
 * Function: <br/>
 * Date: 2018年3月18日 下午3:45:06 <br/>
 */
public interface SubAreaRepository extends JpaRepository<SubArea, Long> {

    // 查询未关联到的分区
    List<SubArea> findByFixedAreaIsNull();

    // 查询已关联指定的分区
    List<SubArea> findByFixedArea(FixedArea fixedAreaId);
}
